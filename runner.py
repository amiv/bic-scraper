from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from ibanscraper.spiders.quotes_spider import QuotesSpider as sp


def run():
    process = CrawlerProcess(get_project_settings())

    process.crawl(sp)

    process.start()


if __name__ == '__main__':
    run()
