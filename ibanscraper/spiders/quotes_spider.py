import scrapy
import settings
import json
from os import path, mkdir

import datetime


class QuotesSpider(scrapy.Spider):
    name = 'iban-scraper'

    def __init__(self):
        self.result = {}

    def start_requests(self):
        urls = []

        for country in settings.COUNTRIES:
            page = 'page/' + str(country['start_page']) if country['start_page'] else ''
            urls.append(f'{settings.BASE_URL}/{country["path"]}/{page}')

        for url in urls:
            self.log("Requesting " + url)
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-2]
        country = response.url.split("/")[-4]

        code = list(filter(lambda c: c['path'] == country, settings.COUNTRIES))[0]['code']

        table = response.css('table.swift')
        entries = table.css('tr')

        first = True

        if code not in self.result:
            self.result[code] = []

        for e in entries:
            if not first:
                if e.css('script').get() is None:
                    items_text = e.css('td::text').getall()[1:]  # ignore number line
                    items_text.append(e.css('td').css('a::text').get())  # treat the BIC specially since it is in a href
                    items = e.css('td').getall()[1:]

                    self.log(items)
                    self.log(items_text)

                    self.result[code].append(QuotesSpider._generate_bank_dict(items_text, items))
            else:
                first = False

        next_page = response.css('span.next a::attr(href)').get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
        else:
            self.log(table)
            self._save_result(code, country, page, response.css('div.g-recaptcha').getall() != [])

    def _save_result(self, code, country, last_page, blocked):
        if not path.isdir(settings.OUTPUT_DIR):
            mkdir(settings.OUTPUT_DIR)

        filename = f'{settings.OUTPUT_DIR}/{code}.json'

        if path.isfile(filename):
            with open(filename, 'r') as old_file:
                existing = json.load(old_file)
            self.result[code] = self._merge_lists(self.result[code], existing)

        with open(filename, 'w') as file:
            json.dump(self.result[code], file, indent=4)

        logfile = f'{settings.OUTPUT_DIR}/{settings.LOG_FILE_NAME}.json'

        log = {}
        if path.isfile(logfile):
            with open(logfile, 'r') as old_file:
                log = json.load(old_file)

        if 'country_queries' not in log:
            log['country_queries'] = {}
        if country not in log['country_queries']:
            log['country_queries'][country] = []

        start_page = list(filter(lambda c: c['path'] == country, settings.COUNTRIES))[0]['start_page']

        log['country_queries'][country].append({'date': str(datetime.datetime.today()),
                                                'first_page': start_page,
                                                'last_page': int(last_page),
                                                'blocked': blocked})

        with open(logfile, 'w') as file:
            json.dump(log, file, indent=4)

    @staticmethod
    def _merge_lists(actual, existing):
        for item in actual:
            found = False
            for index, new_item in enumerate(existing):
                if item['bank_name'] == new_item['bank_name']:
                    found = True
                    existing[index] = {
                        'bank_name': item['bank_name'],
                        'city': item['city'],
                        'branch': item['branch'],
                        'bic': item['bic'],
                    }

                    break
            if not found:
                existing.append({
                    'bank_name': item['bank_name'],
                    'city': item['city'],
                    'branch': item['branch'],
                    'bic': item['bic'],
                })
        return existing

    @staticmethod
    def _generate_bank_dict(texts, items):
        bank_name = QuotesSpider.pick_item(items[0], texts)
        city = QuotesSpider.pick_item(items[1], texts)
        branch = QuotesSpider.pick_item(items[2], texts)
        bic = QuotesSpider.pick_item(items[3], texts)

        bank = {
            'bank_name': bank_name,
            'city': city,
            'branch': branch,
            'bic': bic,
        }

        return bank

    @staticmethod
    def pick_item(item, texts):
        for text in texts:
            if text in item:
                return text
        return None
