BASE_URL = 'https://www.swiftcodes.info/'

OUTPUT_DIR = 'out'

LOG_FILE_NAME = 'log'

COUNTRIES = [
    {
        'path': 'switzerland',
        'code': 'ch',
        'start_page': 18,
    },
]
